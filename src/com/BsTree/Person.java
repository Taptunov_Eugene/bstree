package com.bstree;

public class Person {
    private String firstName;
    private String lastName;
    private int age;
    private String city;

    public Person() {
    }

    public Person(String name, String lastName, int age, String city) {
        this.firstName = name;
        this.lastName = lastName;
        this.age = age;
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Person{firstName='" + firstName + ", lastName='" + lastName + ", age=" + age + ", city='" + city +'}';
    }

    public int compare(Person o1, Person o2) {
        return o1.getFirstName().compareTo(o2.getFirstName());
    }
}
