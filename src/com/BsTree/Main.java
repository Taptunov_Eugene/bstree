package com.bstree;

public class Main {

    public static void main(String[] args) {

        BsTreeR bsTreeR = new BsTreeR();
        BsTreeC bsTreeC = new BsTreeC();

        Person john = new Person("John");
        Person jack = new Person("Jack");
        Person mike = new Person("Mike");
        Person bill = new Person("Bill");
        Person bob = new Person("Bob");
        Person lisa = new Person("Lisa");
        Person teresa = new Person("Teresa");
        Person mia = new Person("Mia");

        bsTreeR.add(jack);
        bsTreeR.add(john);
        bsTreeR.add(mia);
        bsTreeR.add(mike);
        bsTreeR.add(lisa);
        bsTreeR.add(bill);
        bsTreeR.add(bob);
        bsTreeR.add(teresa);

        bsTreeC.add(jack);
        bsTreeC.add(john);
        bsTreeC.add(mia);
        bsTreeC.add(mike);
        bsTreeC.add(lisa);
        bsTreeC.add(bill);
        bsTreeC.add(bob);
        bsTreeC.add(teresa);

        System.out.println(bsTreeR.nodes());

        System.out.println(bsTreeC.nodes());
    }
}
