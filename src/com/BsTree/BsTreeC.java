package com.bstree;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BsTreeC implements BsTree {

    Node root;

    public BsTreeC() {
        root = null;
    }

    public BsTreeC(List<Person> persons) {
        persons.forEach(this::add);
    }

    @Override
    public void clear() {
        root = null;
    }

    @Override
    public int size() {
        if (root == null)
            return 0;
        Queue<Node> queue = new LinkedList<Node>();
        queue.offer(root);
        int count = 1;
        while (!queue.isEmpty()) {
            Node tmp = queue.poll();
            if (tmp != null) {
                if (tmp.left != null) {
                    count++;
                    queue.offer(tmp.left);
                }
                if (tmp.right != null) {
                    count++;
                    queue.offer(tmp.right);
                }
            }
        }
        return count;
    }

    @Override
    public Person[] toArray() {
        Person[] people = new Person[size()];
        int i = 0;
        return putValues(people, i, root);
    }

    private Person[] putValues(Person[] people, int i, Node node) {
        if (node != null) {
            putValues(people, i, node.left);
            people[i] = node.value;
            i++;
            putValues(people, i, node.right);
        }
        return people;
    }

    @Override
    public void add(Person val) {
        insert(root, val);
    }

    public Node insert(Node node, Person value) {
        if (node == null) {
            root = new Node(value);
            return root;
        }
        if (value.compare(value, node.value) < 0) {
            if (node.left != null) {
                insert(node.left, value);
            } else {
                node.left = new Node(value);
            }
        } else if (value.compare(value, node.value) > 0) {
            if (node.right != null) {
                insert(node.right, value);
            } else {
                node.right = new Node(value);
            }
        }
        return node;
    }

    @Override
    public Person del(Person val) {
        Node Current, Parent;
        boolean IsLeftChild = true;
        Current = root;
        Parent = root;
        while (Current.value != val) {
            Parent = Current;
            if (val.compare(val, Current.value) < 0) {
                IsLeftChild = true;
                Current = Current.left;
            } else {
                IsLeftChild = false;
                Current = Current.right;
            }
            if (Current == null)
                return null;
        }

        if (Current.left == null && Current.right == null) {
            if (Current == root)
                root = Current.left;
            else if (IsLeftChild)
                Parent.left = Current.right;
            else
                Parent.right = Current.right;
        } else {
            if (Current.right == null) {
                if (Current == root)
                    root = Current.right;
                else if (IsLeftChild)
                    Parent.left = Current.left;
                else
                    Parent.right = Current.left;
            } else {
                if (Current.left == null) {
                    if (Current == root)
                        root = Current.left;
                    else if (IsLeftChild)
                        Parent.left = Current.right;
                    else
                        Parent.right = Current.right;
                }
            }
        }
        return null;
    }

    @Override
    public int getWidth() {
        return maxWidth(root);
    }

    public static Integer maxWidth(Node root) {
        if (root == null) {
            return 0;
        }
        Queue<Node> queue = new ArrayDeque<>();
        queue.add(root);
        Node current = null;
        int max = 0;
        while (!queue.isEmpty()) {
            int width = queue.size();
            if (max < width) {
                max = width;
            }
            while (width-- > 0) {
                current = queue.poll();
                assert current != null;
                if (current.left != null) {
                    queue.add(current.left);
                }
                if (current.right != null) {
                    queue.add(current.right);
                }
            }
        }
        return max;
    }

    @Override
    public int getHeight() {
        return Height(root);
    }

    public int Height(Node root) {
        Queue<Node> queue = new LinkedList<Node>();
        int height = 0;
        queue.add(root);
        queue.add(null);
        while (queue.isEmpty() == false) {
            Node node = queue.remove();
            if (node == null) {
                if (!queue.isEmpty()) {
                    queue.add(null);
                }
                height++;
            } else {
                if (node.left != null) {
                    queue.add(node.left);
                }
                if (node.right != null) {
                    queue.add(node.right);
                }
            }
        }
        return height;
    }

    @Override
    public int nodes() {
        if (root == null)
            return 0;
        Queue<Node> queue = new LinkedList<Node>();
        queue.add(root);
        int count = 0;
        while (!queue.isEmpty()) {
            Node temp = queue.poll();
            if (temp.left != null && temp.right != null)
                count++;
            if (temp.left != null) {
                queue.add(temp.left);
            }
            if (temp.right != null) {
                queue.add(temp.right);
            }
        }
        return count;
    }

    @Override
    public int leaves() {
        if (root == null) {
            return 0;
        }
        Queue<Node> queue = new LinkedList<Node>();
        queue.add(root);
        int count = 0;
        while (!queue.isEmpty()) {
            Node temp = queue.poll();
            if (temp.left != null && temp.right == null || temp.left == null && temp.right != null) {
                count++;
            }
            if (temp.left != null) {
                queue.add(temp.left);
            }
            if (temp.right != null) {
                queue.add(temp.right);
            }
        }
        return count;
    }
}
