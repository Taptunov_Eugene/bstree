package com.bstree;

public interface BsTree {
        void clear();

        int size();

        Person[] toArray();

        String toString();

        void add(Person val);

        Person del(Person val);

        int getWidth();

        int getHeight();

        int nodes();

        int leaves();
}
