package com.bstree;

import java.util.List;

public class BsTreeR implements BsTree {

    Node root;

    public BsTreeR() {
        root = null;
    }

    public BsTreeR(List<Person> persons) {
        persons.forEach(this::add);
    }

    @Override
    public void clear() {
        root = null;
    }

    public int getTreeSize(Node root) {
        if (root == null) {
            return 0;
        }
        return getTreeSize(root.left) + getTreeSize(root.right) + 1;
    }

    @Override
    public int size() {
        return getTreeSize(root);
    }

    @Override
    public Person[] toArray() {
        return new Person[0];
    }

    private Node addRecursive(Node current, Person value) {
        if (current == null) {
            return new Node(value);
        }
        if (value.getFirstName().compareTo(current.value.getFirstName()) < 0) {
            current.left = addRecursive(current.left, value);
        } else if (value.getFirstName().compareTo(current.value.getFirstName()) > 0) {
            current.right = addRecursive(current.right, value);
        } else {
            return current;
        }
        return current;
    }

    @Override
    public void add(Person val) {
        root = addRecursive(root, val);
    }

    private Node deleteRecursive(Node current, Person value) {
        if (current == null) {
            return null;
        }
        if (value.equals(current.value)) {
            if (current.left == null && current.right == null) {
                return null;
            }
            if (current.right == null) {
                return current.left;
            }
            if (current.left == null) {
                return current.right;
            }
            Person smallestValue = findSmallestValue(current.right);
            current.value = smallestValue;
            current.right = deleteRecursive(current.right, smallestValue);
            return current;
        }
        if (value.getLastName().compareTo(current.value.getLastName()) < 0) {
            current.left = deleteRecursive(current.left, value);
            return current;
        }
        current.right = deleteRecursive(current.right, value);
        return current;
    }

    private Person findSmallestValue(Node root) {
        return root.left == null ? root.value : findSmallestValue(root.left);
    }

    @Override
    public Person del(Person val) {
        root = deleteRecursive(root, val);
        return val;
    }

    private int maxDepth(Node node) {
        if (node == null) {
            return 0;
        } else {
            int lDepth = maxDepth(node.left);
            int rDepth = maxDepth(node.right);
            if (lDepth > rDepth) {
                return (lDepth + 1);
            } else {
                return (rDepth + 1);
            }
        }
    }

    private int getMaxWidth(Node node) {
        int maxWidth = 0;
        int width;
        int h = height(node);
        int i;

        for (i = 1; i <= h; i++) {
            width = getWidth(node, i);
            if (width > maxWidth)
                maxWidth = width;
        }

        return maxWidth;
    }

    private int height(Node node) {
        if (node == null)
            return 0;
        else {

            int lHeight = height(node.left);
            int rHeight = height(node.right);

            return (lHeight > rHeight) ? (lHeight + 1) : (rHeight + 1);
        }
    }

    private int getWidth(Node node, int level) {
        if (node == null)
            return 0;

        if (level == 1)
            return 1;
        else if (level > 1)
            return getWidth(node.left, level - 1)
                    + getWidth(node.right, level - 1);
        return 0;
    }

    @Override
    public int getWidth() {
        return getMaxWidth(this.root);
    }

    @Override
    public int getHeight() {
        return maxDepth(this.root);
    }

    @Override
    public int nodes() {
        return getTreeNodesCount(root) - getTreeLeavesCount(root);
    }

    private int getTreeNodesCount(Node node) {
        if (node == null) {
            return 0;
        }
        return getTreeNodesCount(node.left) + getTreeNodesCount(node.right) + 1;
    }

    @Override
    public int leaves() {
        return getTreeLeavesCount(root);
    }

    private int getTreeLeavesCount(Node node) {
        if (node == null) {
            return 0;
        } else if (node.left == null && node.right == null) {
            return 1;
        } else {
            return getTreeLeavesCount(node.left) + getTreeLeavesCount(node.right);
        }
    }
}
